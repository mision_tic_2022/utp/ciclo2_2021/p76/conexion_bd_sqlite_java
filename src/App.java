import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

public class App {
    public static void main(String[] args) throws Exception {
        // Crear objeto de conexion a la BD
        Connection conn = DriverManager.getConnection("jdbc:sqlite:hr.db");
        // Variable para ejecutar sentencias sql
        Statement statement = conn.createStatement();
        // Consultas
        //ResultSet employees = statement.executeQuery("SELECT * FROM employees ORDER BY salary DESC");
        int id = 100;
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM employees WHERE employee_id = ?");
        ps.setInt(1, id);
        //Ejecutar la sentencia sql
        ResultSet employees = ps.executeQuery();
        // Itero los registros
        while (employees.next()) {
            System.out.println(employees.getInt("employee_id") + "-" + employees.getString("first_name"));
        }

    }
}
